package com.drmarkdown.auth.services.impl;

import com.drmarkdown.auth.exceptions.InvalidTokenException;
import com.drmarkdown.auth.models.MarkdownUserModel;
import com.drmarkdown.auth.repositories.UserRepository;
import com.drmarkdown.auth.services.AuthSigninKeyResolver;
import com.drmarkdown.auth.services.TokenService;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.SignatureException;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@Service
public class TokenServiceImpl implements TokenService {

    private AuthSigninKeyResolver authSigninKeyResolver;

    @Autowired
    private UserRepository userRepository;

    @Override
    public boolean validateToken(String jwtToken) throws InvalidTokenException {
        try {
            // Check token not null
            if (jwtToken == null)
                return false;

            // Parse token
            var jwtTokenObj = Jwts.parserBuilder()
                    .setSigningKeyResolver(authSigninKeyResolver)
                    .build().parse(jwtToken);
            var map = (Map<String, Object>) jwtTokenObj.getBody();
            var exp = (Long) map.get("exp");

            // Check token not expired
            return System.currentTimeMillis() <= exp;
        } catch (ExpiredJwtException | MalformedJwtException | SignatureException | IllegalArgumentException e) {
            throw new InvalidTokenException("Invalid token", e);
        }
    }

    @Override
    public void generateToken(MarkdownUserModel markdownUserModel) {
        //TODO: Replace token with JWT
        var claims = new HashMap<String, Object>();
        claims.put("iss", markdownUserModel.getId());
        claims.put("exp", System.currentTimeMillis() + (1000 * 60 * 60 * 12));
        claims.put("iat", System.currentTimeMillis());
        claims.put("sub", markdownUserModel.getUsername());
        claims.put("aud", markdownUserModel.getRoles().toString());

        String jwtToken = Jwts.builder()
                .setClaims(claims)
                .signWith(authSigninKeyResolver.getSecretKey(), SignatureAlgorithm.HS512)
                .compact();
        if (validateToken(markdownUserModel.getJwtToken())) {
            markdownUserModel.setJwtToken(jwtToken);
        } else {
            markdownUserModel.setJwtToken(jwtToken);
            userRepository.save(markdownUserModel);
        }
    }
}
